import {
  JupyterFrontEnd,
  JupyterFrontEndPlugin
} from '@jupyterlab/application';

import { ISettingRegistry } from '@jupyterlab/settingregistry';

import { 
  //NotebookActions,
  NotebookModel, 
  INotebookTracker 
} from '@jupyterlab/notebook';

import { PanelLayout } from '@lumino/widgets';

import { Cell, ICellModel } from "@jupyterlab/cells";

/**
 * Initialization data for the lnb_rw_undoredo extension.
 */
const plugin: JupyterFrontEndPlugin<void> = {
  id: 'lnb_rw_undoredo:plugin',
  autoStart: true,
  optional: [ISettingRegistry],
  requires: [INotebookTracker],
  activate: (app: JupyterFrontEnd, notebookTracker:INotebookTracker, settingRegistry: ISettingRegistry | null) => {

    //const id_ld: any = window.frameElement.id.split('ld_code_iframe_').pop();

    if (settingRegistry) {
      settingRegistry
        .load(plugin.id)
        .then(settings => {
          console.log('lnb_rw_undoredo settings loaded:', settings.composite);
        })
        .catch(reason => {
          console.error('Failed to load settings for lnb_rw_undoredo.', reason);
        });
    }

    notebookTracker.currentChanged.connect((tracker, panel) => {
        function undo_redo(event: any){
            if (event.data.type === 'undo_redo_notebook') {
		        console.log('lnb_rw_undoredo -> lnb: update_content [id_labdoc='+event.data.id_ld+']');
                if (panel !== null){
                    // 1 - Update content
                    const model = new NotebookModel();
                    model.fromJSON(JSON.parse(event.data.ld_content));
                    panel.content.model = model;

                    // 2 - Hide utils_function cell
                    const tag = "utils_functions";
                    panel.content.widgets.forEach((cell: Cell<ICellModel>, index: number) => {
                        let arrayOfTags = cell.model.metadata.get("tags");
                        if (Array.isArray(arrayOfTags) && arrayOfTags != null && arrayOfTags != undefined){
                            if (arrayOfTags.includes(tag)){
                                let layout = panel.content.widgets[index].layout as unknown as PanelLayout;
                                for (var i=0; i<5; i++){
                                    layout.widgets[i].hide();
                                }
                                if (typeof document.querySelectorAll('div.jp-Cell-inputWrapper[aria-hidden="true"]')[0] !== 'undefined'){
                                    (document.querySelectorAll('div.jp-Cell-inputWrapper[aria-hidden="true"]')[0]
                                        .closest('div.jp-Cell') as HTMLElement)
                                        .style.display = 'none';
                                }
                            }
                        }
                    });

                } else {
                    console.error('lnb_rw_undoredo -> lnb: error while undo redo the notebook [id_labdoc='+event.data.id_ld+']');
                }
	        }
        }

        window.addEventListener('message', undo_redo, {once: false});
    });

  }
};

export default plugin;
